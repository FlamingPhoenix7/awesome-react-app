import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

export const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: "awesomeness-dd93e.firebaseapp.com",
  databaseURL: "https://awesomeness-dd93e.firebaseio.com",
  projectId: "awesomeness-dd93e",
  storageBucket: "awesomeness-dd93e.appspot.com",
  messagingSenderId: "169645329687",
  appId: "1:169645329687:web:75b611564a2265a53b7b52",
  measurementId: "G-MKZ2V3279N",
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const db = firebase.firestore();

export default firebase;
