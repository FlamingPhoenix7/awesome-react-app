import { db, auth } from "../../config/firebase";

export const [ADD_USER, ADD_USER_PHOTO, MODIFY_USER, AUTH_STATE_CHANGED] = [
  "ADD_USER",
  "ADD_USER_PHOTO",
  "MODIFY_USER",
];

/**
 * Creates a new user using
 * - Name
 * - Username
 * - Email
 * - Password
 *
 * Saves the newly created user in users collection
 *
 * @param name The user's full name
 * @param username The user's username
 * @param email The user's email address
 * @param password The user's password
 */
export const signupAction = (name, username, email, password) => {
  return (dispatch) => {
    // Create the user
    auth.createUserWithEmailAndPassword(email, password).then(({ user }) => {
      // Add user extra info to database
      db.collection("users").doc(user.uid).set({
        username,
        name,
        email,
        photoURL: null,
      });
    });
  };
};

/**
 * Signs in a user
 *
 * @param email The newly signed in user's email
 * @param password The newly signed in user's password
 */
export const signinAction = (email, password) => {
  return (dispatch) => {
    // Sign in the user
    auth.signInWithEmailAndPassword(email, password);
  };
};
