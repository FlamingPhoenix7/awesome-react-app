import { db } from "../../config/firebase";

export const [ADD_POST, DELETE_POST] = ["ADD_POST", "DELETE_POST"];

/**
 * Creates a new post in the posts collection
 *
 * @param post The post to add to posts collection
 */
export const addPostAction = (post) => {
  return (dispatch) => {
    db.collection("posts").add(post);
  };
};

/**
 * Deletes a post from the posts collection
 *
 * @param postId The id of the post to be deleted from posts collection
 */
export const deletePostAction = (postId) => {
  return (dispatch) => {
    db.collection("posts").doc(postId).delete();
  };
};
