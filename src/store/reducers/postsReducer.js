import { ADD_POST, DELETE_POST } from "../actions/postsActions";

const initState = {
  posts: [],
};

const postsReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_POST:
      return {
        ...state,
        posts: [...state.posts, action.post],
      };

    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== action.postId),
      };

    default:
      return state;
  }
};

export default postsReducer;
