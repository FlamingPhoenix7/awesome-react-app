import {
  ADD_USER,
  MODIFY_USER,
  ADD_USER_PHOTO,
  AUTH_STATE_CHANGED,
} from "../actions/usersActions";

const initState = {
  users: [],
  currentUser: undefined,
};

const usersReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_USER:
      return {
        ...state,
        users: [...state.users, action.user],
      };

    case MODIFY_USER:
      // Delete the modified user's old data
      const users = state.users.filter((user) => user.uid !== action.user.uid);
      return {
        ...state,
        users: [...users, action.user], // Add the modified user's new data
      };

    case AUTH_STATE_CHANGED:
      return {
        ...state,
        currentUser: action.user,
      };

    default:
      return state;
  }
};

export default usersReducer;
