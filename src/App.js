import React, { Component } from "react";
import { auth, db } from "./config/firebase";
import {
  AUTH_STATE_CHANGED,
  ADD_USER,
  MODIFY_USER,
} from "./store/actions/usersActions";
import { DELETE_POST, ADD_POST } from "./store/actions/postsActions";
import { connect } from "react-redux";

class App extends Component {
  /**
   * Listens for changes to the auth state
   * i.e. Current user state and updates the
   * Redux store accordingly
   */
  authStateChangeListener = () => {
    auth.onAuthStateChanged((user) => {
      this.props.changeAuthState(user);
    });
  };

  /**
   * Listens for changes to the users collection
   * Updates Redux store accordingly
   */
  usersListener = () => {
    db.collection("users").onSnapshot((snapshot) => {
      snapshot.docChanges().forEach((change) => {
        const user = { ...change.doc.data(), uid: change.doc.id };
        switch (change.type) {
          case "added":
            this.props.addUser(user);
            break;

          case "modified":
            this.props.modifyUser(user);
            break;

          default:
            break;
        }
      });
    });
  };

  /**
   * Listens for changes to the posts collection
   * Updates Redux store accordingly
   */
  postsListener = () => {
    db.collection("posts").onSnapshot((snapshot) => {
      snapshot.docChanges().forEach((change) => {
        switch (change.type) {
          case "added":
            this.props.addPost({ ...change.doc.data(), id: change.doc.id });
            break;

          case "removed":
            this.props.deletePost(change.doc.id);
            break;

          default:
            break;
        }
      });
    });
  };

  componentDidMount() {
    this.authStateChangeListener();
    this.usersListener();
    this.postsListener();
  }

  render() {
    return <div className="App"></div>;
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
  posts: state.posts,
});

const mapDispatchToProps = (dispatch) => ({
  // ============ USER =================
  changeAuthState: (user) => {
    dispatch({ type: AUTH_STATE_CHANGED, user });
  },

  addUser: (user) => {
    dispatch({ type: ADD_USER, user });
  },

  modifyUser: (user) => {
    dispatch({ type: MODIFY_USER, user });
  },

  // ============ POSTS =================
  addPost: (post) => {
    dispatch({ type: ADD_POST, post });
  },

  deletePost: (postId) => {
    dispatch({ type: DELETE_POST, postId });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
